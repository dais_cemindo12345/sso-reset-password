const visibilityOffNewPassword = document.getElementById('visibility_off_new_password');
const visibilityOffConfirmPassword = document.getElementById('visibility_off_confirm_password');
const inputNewPassword = document.getElementById('new-password');
const inputConfirmPassword = document.getElementById('confirm-password');
const handleText = document.getElementById('handle-text');
const handlePassword = document.getElementById('handle-password');
const handlePasswordLock = document.getElementById('handle-password-lock');
const char8 = document.getElementById('char8');
const upperAndLower = document.getElementById('upper-and-lower');
const useNumber = document.getElementById('use-number');
const specialChar = document.getElementById('special-char');
const submitNewPassword = document.getElementById('submit-new-password');

visibilityOffNewPassword.addEventListener('click', () => {

  if (visibilityOffNewPassword.innerHTML === 'visibility_off') {
    visibilityOffNewPassword.innerHTML = 'visibility';
    inputNewPassword.removeAttribute('type', 'password');
    inputNewPassword.setAttribute('type', 'text');
  } else {
    visibilityOffNewPassword.innerHTML = 'visibility_off';
    inputNewPassword.removeAttribute('type', 'text');
    inputNewPassword.setAttribute('type', 'password');
  }
})

visibilityOffConfirmPassword.addEventListener('click', () => {

  if (visibilityOffConfirmPassword.innerHTML === 'visibility_off') {
    visibilityOffConfirmPassword.innerHTML = 'visibility';
    inputConfirmPassword.removeAttribute('type', 'password');
    inputConfirmPassword.setAttribute('type', 'text');
  } else {
    visibilityOffConfirmPassword.innerHTML = 'visibility_off';
    inputConfirmPassword.removeAttribute('type', 'text');
    inputConfirmPassword.setAttribute('type', 'password');
  }
})

inputNewPassword.addEventListener('keyup', (e) => {
  // Not null
  if(inputNewPassword.value.length > 0) {
    handleText.classList.add('hidden');
    handlePassword.classList.remove('hidden');
  }

  // Validate length
  if(inputNewPassword.value.length >= 8) {
    char8.innerHTML = 'check_circle';
    char8.classList.remove('text-red-500');
    char8.classList.add('text-green-500');
  } else {
    char8.innerHTML = 'cancel';
    char8.classList.remove('text-green-500');
    char8.classList.add('text-red-500');
  }

  // Validate lowercase letters
  var lowerCaseLetters = /[a-z]/g;
  var upperCaseLetters = /[A-Z]/g;
  if(inputNewPassword.value.match(lowerCaseLetters) && inputNewPassword.value.match(upperCaseLetters)) {  
    upperAndLower.innerHTML = 'check_circle';
    upperAndLower.classList.remove('text-red-500');
    upperAndLower.classList.add('text-green-500');
  } else {
    upperAndLower.innerHTML = 'cancel';
    upperAndLower.classList.remove('text-green-500');
    upperAndLower.classList.add('text-red-500');
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(inputNewPassword.value.match(numbers)) {  
    useNumber.innerHTML = 'check_circle';
    useNumber.classList.remove('text-red-500');
    useNumber.classList.add('text-green-500');
  } else {
    useNumber.innerHTML = 'cancel';
    useNumber.classList.remove('text-green-500');
    useNumber.classList.add('text-red-500');
  }

  // Validate special char
  var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  if(inputNewPassword.value.match(format)) {  
    specialChar.innerHTML = 'check_circle';
    specialChar.classList.remove('text-red-500');
    specialChar.classList.add('text-green-500');
  } else {
    specialChar.innerHTML = 'cancel';
    specialChar.classList.remove('text-green-500');
    specialChar.classList.add('text-red-500');
  }

  if (inputNewPassword.value.length > 0 && inputNewPassword.value.length >= 8 && inputNewPassword.value.match(lowerCaseLetters) && inputNewPassword.value.match(upperCaseLetters) && inputNewPassword.value.match(numbers) && inputNewPassword.value.match(format)) {
    handlePassword.classList.add('hidden');
    handlePasswordLock.classList.remove('hidden');
    submitNewPassword.setAttribute('type', 'submit');;
    submitNewPassword.removeAttribute('disabled');
    submitNewPassword.classList.remove('bg-gray-200');
    submitNewPassword.classList.add('bg-red-700');
  } else {
    handlePassword.classList.remove('hidden');
    handlePasswordLock.classList.add('hidden');

  }
})