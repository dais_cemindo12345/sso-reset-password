<div class="absolute w-11/12 md:w-8/12 lg:w-6/12 2xl:w-4/12 bg-white rounded-md shadow-md left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2">
  <div class="px-4 py-8 md:px-16 md:py-12">
    <div class="text-center">
      <img class="mx-auto" src="<?= base_url() ?>assets/images/icon-expired.png" alt="icon-expired.png">

      <div class="mt-9 w-full mx-auto">
        <h1 class="text-red-800 text-2xl font-bold">Link Reset Password Expired</h1>
      </div>

      <div class="my-8">
        <p class="text-gray-500">
          This link is no longer active. If you want to reset your password please click on the button below
        </p>
      </div>

      <div class="block">
        <a href="<?= base_url() ?>" class="block bg-red-800 text-center px-4 py-5 rounded-md text-white font-bold hover:bg-red-900">Reset
          Password</a>
      </div>
    </div>
  </div>
</div>