<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- My CSS -->
  <link rel="stylesheet" href="<?= base_url() ?>custom/css/style.min.css">

  <!-- Google Icon CDN -->
  <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined" rel="stylesheet">

  <title><?= $title; ?></title>
</head>

<body>
  <div class="bg-[url('./../../assets/images/Pattern.png')] bg-cover w-screen h-screen relative overflow-auto">