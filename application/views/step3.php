<div class="md:absolute md:my-8 w-11/12 md:w-8/12 lg:w-6/12 2xl:w-4/12 bg-white rounded-md shadow-md mx-auto my-8 md:left-1/2 md:top-1/2 md:transform md:-translate-x-1/2 md:-translate-y-1/2">
  <div class="px-4 py-8 md:px-16 md:py-11">
    <div class="text-center">
      <p class="text-gray-400">Single Sign On (SSO)</p>
      <h1 class="text-3xl font-bold">Create new password</h1>
    </div>

    <div class="w-6/12 mx-auto h-0.5 bg-gray-200 rounded mt-6"></div>

    <!-- <div class="mt-8">
      <div class="p-6 border border-gray-200">
        <div class="font-medium">fansuri.arrumi@cemindo.com</div>
      </div>
    </div> -->

    <div class="mt-8">
      <form action="<?= base_url() ?>reset_password_sso/send_reset_password" method="POST">
        <input type="hidden" name="token" id="token" value="<?= $token; ?>">
        <input type="hidden" name="current-url" id="current-url" value="<?= current_url(); ?>">

        <div class="mb-4">
          <label for="email" class="block mb-2">Email</label>
          <input type="email" id="email" name="email" class="block border border-slate-300 rounded-md w-full pl-4 pr-12 py-5 focus:outline-none focus:border-sky-500" placeholder="Enter your confirm password" value="<?php echo set_value('email'); ?>">
        </div>
        <?= form_error('email', '<small class="text-red-500">', '</small>') ?>
    </div>

    <div class="mb-4">
      <label for="new-password" class="block mb-2">New Password</label>
      <div class="relative">
        <input type="password" id="new-password" name="new-password" class="block border border-slate-300 rounded-md w-full pl-4 pr-12 py-5 focus:outline-none focus:border-sky-500" placeholder="Enter your new password" required>
        <div class="absolute inset-y-0 right-0 flex items-center">
          <div class="px-4">
            <span id="visibility_off_new_password" class="material-icons-outlined text-gray-500 hover:text-gray-600 hover:cursor-pointer">visibility_off</span>
          </div>
        </div>
      </div>
      <small id="handle-text" class="text-gray-400">Must be at least 8 characters</small>
      <div id="handle-password" class="p-4 bg-gray-100 rounded-md mt-4 hidden">
        <div class="check flex">
          <span id="char8" class="material-icons-outlined text-red-500 mr-2 mb-2">cancel</span> Use at least 8 characters
        </div>
        <div class="check flex">
          <span id="upper-and-lower" class="material-icons-outlined text-red-500 mr-2 mb-2">cancel</span> Use upper and lower case characters (Aa)
        </div>
        <div class="check flex">
          <span id="use-number" class="material-icons-outlined text-red-500 mr-2 mb-2">cancel</span> Use at least 1 number (0-9)
        </div>
        <div class="check flex">
          <span id="special-char" class="material-icons-outlined text-red-500 mr-2 mb-2">cancel</span> Use special characters (!@#$%^&*)
        </div>
      </div>
      <div id="handle-password-lock" class="p-4 bg-gray-100 rounded-md mt-4 hidden">
        <div class="flex items-center">
          <span class="material-icons-outlined mr-2">lock</span>
          <span>You have created a strong password.</span>
        </div>
      </div>
    </div>

    <div class="mb-4">
      <label for="confirm-password" class="block mb-2">Confirm Password</label>
      <div class="relative">
        <input type="password" id="confirm-password" name="confirm-password" class="block border border-slate-300 rounded-md w-full pl-4 pr-12 py-5 focus:outline-none focus:border-sky-500" placeholder="Enter your confirm password" required>
        <div class="absolute inset-y-0 right-0 flex items-center">
          <div class="px-4">
            <span id="visibility_off_confirm_password" class="material-icons-outlined text-gray-500 hover:text-gray-600 hover:cursor-pointer ">visibility_off</span>
          </div>
        </div>
      </div>
      <?php if (!empty($this->session->flashdata('confirm'))) : ?>
        <small class="text-red-500"><?= $this->session->flashdata('confirm') ?></small>
      <?php endif; ?>
    </div>

    <div class="block">
      <button id="submit-new-password" type="button" class="w-full bg-gray-200 hover:bg-gray-200 text-center px-4 py-5 rounded-md text-white font-bold" disabled>Reset
        Password</button>
    </div>
    <div class="bg-red-700 hover:bg-red-800 text-green-500"></div>
    </form>
  </div>
</div>
</div>

<script src="<?= base_url() ?>custom/js/step3.js"></script>