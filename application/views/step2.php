<!--  if add margin md:my-8 -->
<div class="absolute w-11/12 md:w-8/12 lg:w-6/12 2xl:w-4/12 bg-white rounded-md shadow-md left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2">
  <div class="px-4 py-8 md:px-16 md:py-11">
    <div class="text-center">
      <img class="mx-auto" src="<?= base_url() ?>assets/images/icon-message-key.png" alt="icon-message-key.png">

      <div class="mt-9">
        <p class="text-gray-500">Reset Password</p>
        <h1 class="text-red-800 text-2xl font-bold">Check your email</h1>
      </div>

      <div class="w-6/12 mx-auto h-0.5 bg-gray-200 rounded mt-6"></div>

      <div class="mt-9">
        <p class="text-gray-500">We have sent you a password recover to your email:</p>

        <div class="my-4">
          <div class="inline-block py-3 px-4 bg-gray-100 text-gray-600 rounded-full">
            <div class="flex">
              <span class="material-icons-outlined text-gray-500">
                email
              </span>
              <p class="ml-2 text-gray-500"><?= $this->session->userdata('email'); ?></p>
            </div>
          </div>
        </div>

        <p class="text-gray-500">Didn’t receive the email? Please check your spam folder or
          <?php if (!empty($this->session->tempdata('resend'))) : ?>
        <p class="disabled:text-gray-800 font-bold cursor-default">Resend a new link</p>
      <?php else : ?>
        <?php
            $encryp_email = $this->Encrypt_decrypt->encrypt($this->session->userdata('email'));
        ?>
        <a href="<?= base_url() ?>reset_password_sso/resend_a_new_link?e=<?= $encryp_email; ?>" class="text-red-800 font-bold">Resend a new link</a>
      <?php endif; ?>
      </p>
      </div>
    </div>
  </div>
</div>

<!-- handling -->
<?php if (!empty($this->session->flashdata('errAPI'))) : ?>
  <p class="absolute p-4 rounded-md bg-gray-200 transform -translate-x-1/2 left-1/2 bottom-6">
    <?= $this->session->flashdata('errAPI'); ?>
  </p>
<?php endif; ?>

<?php if (!empty($this->session->tempdata('resend'))) : ?>
  <p class="absolute flex items-center p-4 rounded-md bg-gray-200 transform -translate-x-1/2 left-1/2 bottom-6">
    <span class="material-icons-outlined text-red-500 mr-3">
      email
    </span>
    <?= $this->session->tempdata('resend'); ?>
    </div>
  </p>
<?php endif; ?>