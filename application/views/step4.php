<div class="absolute w-11/12 md:w-8/12 lg:w-6/12 2xl:w-4/12 bg-white rounded-md shadow-md left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2">
  <div class="px-4 py-8 md:px-16 md:py-12">
    <div class="text-center">
      <div class="mt-9 w-full mx-auto">
        <p class="text-gray-500">Your account is now recovered</p>
        <h1 class="text-red-800 text-2xl font-bold">Reset Password Success</h1>
      </div>

      <div class="my-8">
        <p class="text-gray-500">
          Please keep in mind to remember your password next time. It is safe to close this page.
        </p>
      </div>

      <div class="block">
        <a href="#" class="block bg-red-800 text-center px-4 py-5 rounded-md text-white font-bold hover:bg-red-900">I
          understand</a>
      </div>
    </div>
  </div>
</div>