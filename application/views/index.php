<div class="absolute w-11/12 md:w-8/12 lg:w-6/12 2xl:w-4/12 bg-white rounded-md shadow-md left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2">
  <div class="sm:px-10 md:px-16 md:py-11">
    <div class="text-center">
      <p class="text-gray-400">Single Sign On (SSO)</p>
      <h1 class="text-3xl font-bold">Forgot Password</h1>
    </div>

    <div class="mt-8">
      <form action="<?= base_url() ?>reset_password_sso" method="POST">
        <div class="mb-4">
          <label for="employee-email" class="block mb-2">Email</label>
          <input type="email" id="employee-email" name="employee-email" class="block border border-slate-300 rounded-md w-full px-4 py-5 focus:outline-none focus:border-sky-500" placeholder="Enter your email" value="<?php echo set_value('employee-email'); ?>">
          <?php if (empty(validation_errors()) && empty($this->session->flashdata('employee-email'))) : ?>
            <small class="text-gray-400">Use email registered with HCIS</small>
          <?php endif; ?>
          <small class="text-red-500">
            <?= $this->session->flashdata('employee-email'); ?>
          </small>
          <?php echo form_error('employee-email', '<small class="text-red-500">', '</small>'); ?>
        </div>

        <div class="block">
          <button type="submit" class="w-full bg-red-800 text-center px-4 py-5 rounded-md text-white font-bold hover:bg-red-900">Next</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php if (!empty($this->session->flashdata('errAPI'))) : ?>
  <p class="absolute p-4 rounded-md bg-gray-200 transform -translate-x-1/2 left-1/2 bottom-6">
    <?= $this->session->flashdata('errAPI'); ?>
  </p>
<?php endif; ?>