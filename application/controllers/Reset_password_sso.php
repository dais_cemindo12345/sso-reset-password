<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reset_password_sso extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    // Your own constructor code

    // load library
    $this->load->library('encryption');
    $this->load->library('form_validation');

    // load model
    $this->load->model('Encrypt_decrypt_model', 'Encrypt_decrypt');
    $this->load->model('Reset_password_sso_model', 'Reset_password_sso');
    $this->load->model('Api_model', 'Api');
  }

  public function index()
  {
    // set validation
    $this->form_validation->set_rules(
      'employee-email',
      'Email',
      'trim|required|valid_email',
      [
        'required'      => 'You have not provided %s',
      ]
    );


    if ($this->form_validation->run() == FALSE) {
      $data['title'] = "SSO - Reset Password";
      $this->load->view('templates/header', $data);
      $this->load->view('index');
      $this->load->view('templates/footer');
    } else {
      // check data user
      $this->send_data_user();
    }
  }

  public function send_data_user()
  {

    $email = $this->input->post('employee-email');

    // data forgot password
    $data_forgot_password = $this->Reset_password_sso->forgot_password($email);

    // send fata_forgot_password to api
    $send_forgot_password = $this->Api->post($data_forgot_password['url'], $data_forgot_password['params']);

    if ($send_forgot_password['success'] == false) {
      $this->session->set_flashdata('employee-email', $send_forgot_password['message']);
      redirect('reset_password_sso');
    } else {
      $this->session->set_userdata('email', $email);
      redirect('reset_password_sso/step_2');
    }
  }

  public function step_2()
  {
    $data['title'] = "SSO - Reset Password";
    $this->load->view('templates/header', $data);
    $this->load->view('step2');
    $this->load->view('templates/footer');
  }

  public function resend_a_new_link()
  {
    // resend data
    $encrypt_email = $this->input->get('e');

    $email = $this->Encrypt_decrypt->decrypt($encrypt_email);

    // data forgot password
    $data_forgot_password = $this->Reset_password_sso->forgot_password($email);

    // send fata_forgot_password to api
    $send_forgot_password = $this->Api->post($data_forgot_password['url'], $data_forgot_password['params']);

    if ($send_forgot_password == false) {
      $this->session->set_flashdata('errAPI', 'Something wronng on system !');
      redirect('reset_password_sso/step_2');
    }

    // $this->session->set_userdata('email', $email);

    $this->session->set_tempdata('resend', 'New activation link has been send to your email', 60);

    redirect('reset_password_sso/step_2');
  }

  public function step_3()
  {
    $token = $this->uri->segment(3);

    if (empty($token)) {
      redirect('reset_password_sso');
      exit;
    }

    $data['title'] = "SSO - Reset Password";
    $data['token'] = $token;
    $this->load->view('templates/header', $data);
    $this->load->view('step3', $data);
    $this->load->view('templates/footer');
  }

  public function send_reset_password()
  {
    $post = $this->input->post();

    $this->form_validation->set_rules(
      'email',
      'Email',
      'required|valid_email',
    );
    $this->form_validation->set_rules(
      'new-password',
      'New Password',
      'required',
    );
    $this->form_validation->set_rules(
      'confirm-password',
      'Confirm Password',
      'required|matches[new-password]',
    );

    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('confirm', form_error('confirm-password'));
      redirect($post['current-url']);
    } else {
      $params = [
        'email' => $post['email'],
        'token' => $post['token'],
        'password' => $post['new-password'],
        'password_confirmation' => $post['confirm-password']
      ];

      $result = $this->Reset_password_sso->send_reset_password($params);

      if (!empty($result['success']) == true) {
        # success
        redirect('reset_password_sso/step_4');
      } elseif (!empty($result['success']) == false) {
        # expired
        redirect('reset_password_sso/expired');
      } else {
        #something wrong
        $this->session->set_flashdata('errAPI', $result['message']);
        redirect('reset_password_sso');
      }
    }
  }

  public function expired()
  {
    $data['title'] = "SSO - Reset Password";
    $this->load->view('templates/header', $data);
    $this->load->view('expired', $data);
    $this->load->view('templates/footer');
  }

  public function step_4()
  {
    $data['title'] = "SSO - Reset Password";
    $this->load->view('templates/header', $data);
    $this->load->view('step4', $data);
    $this->load->view('templates/footer');
  }
}
