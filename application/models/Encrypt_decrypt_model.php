<?php

class Encrypt_decrypt_model extends CI_Model
{

  public function encrypt($data)
  {
    $this->encryption->initialize(
      [
        'cipher' => 'aes-128',
        'mode' => 'ctr',
        'key' => 'sj kaienl93makf19?:ks1asdfj'
      ]
    );

    $encrypt = $this->encryption->encrypt($data);

    return $encrypt;
  }

  public function decrypt($data)
  {
    $this->encryption->initialize(
      [
        'cipher' => 'aes-128',
        'mode' => 'ctr',
        'key' => 'sj kaienl93makf19?:ks1asdfj'
      ]
    );

    $decrypt = $this->encryption->decrypt($data);

    return $decrypt;
  }
}
