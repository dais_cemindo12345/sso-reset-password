<?php

class Api_model extends CI_Model
{
  private $baseUrl = "http://10.10.10.16:8000/api/"; //SSO api base_url

  public function post($url, $params)
  {
    // $param = json_encode($params);
    $curl = curl_init($this->baseUrl . $url);

    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      "Accept: application/json",
      'Authorization: Bearer e783df8c-7b99-4591-bd04-7d04d0718b85' // salome
      // 'Authorization: Bearer 4d5708ef7640cd39dea3a7872ac8da346d05b080' // prod
    ));

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
    curl_setopt($curl, CURLOPT_POSTFIELDS, $params);  // Insert the data

    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

    // Send the request
    $result = curl_exec($curl);
    // Free up the resources $curl is using
    curl_close($curl);
    $data = json_decode($result, true);
    return $data;
  }
}
