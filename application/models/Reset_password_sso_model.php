<?php

class Reset_password_sso_model extends CI_Model
{

  public function forgot_password($email)
  {
    $url = 'forgot-password';
    $params = [
      'email' => $email,
      'link' => base_url() . 'reset_password_sso/step_3'
    ];

    $data = [
      'url' => $url,
      'params' => $params,
    ];

    return $data;
  }

  public function send_reset_password($params)
  {
    $url = 'reset-password';

    $send_reset_password = $this->Api->post($url, $params);

    return $send_reset_password;
  }
}
